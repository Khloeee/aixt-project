// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: os.c
// Author: Fernando Martínez Santa
// Date: 2023
// License: MIT
//
// Description: This module includes all the OS functions for the Aixt 
//              PC port. 
#include "./os/input.c"